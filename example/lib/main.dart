import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast_provider/toast_provider.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void _showToastMessage() {
    ToastProvider.showShortToast("我是Toast消息");
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Plugin example app'),
        ),
        body: new Center(
          child: new RaisedButton(onPressed: _showToastMessage),
        ),
      ),
    );
  }
}
